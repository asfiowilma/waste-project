------------------------

-- AUTH SEED

------------------------

INSERT INTO auth_user_comp (id) VALUES (487438608);

INSERT INTO
    auth_user_impl(
        id,
        allowedpermissions,
        email,
        name
    )
VALUES (
        487438608,
        'administrator',
        'litha@litha.com',
        'Litha'
    );

INSERT INTO
    auth_user_passworded(id, password, user_id)
VALUES (
        487438608,
        '29afc5930d0fe66ff4e566ebcb39483c8d8a9836867b53f5c3383582782cd749bf4e0be5f43f7ebd395ee61cbab227559c319870d95449ecd7e0e7ef3ad7a8df',
        487438608
    );

INSERT INTO auth_role_comp (id) VALUES (1), (2), (3), (4), (5), (6);

INSERT INTO
    auth_role_impl (id, name, allowedPermissions)
VALUES (
        1,
        'Administrator',
        'administrator'
    ), (2, 'Registered', ''), (
        3,
        'Keuangan',
        'CreateIncome,UpdateIncome,DeleteIncome,CreateExpense,UpdateExpense,DeleteExpense,CreateFinancialReport,UpdateFinancialReport,DeleteFinancialReport'
    ), (
        4,
        'Humas',
        'CreateProgram,UpdateProgram,DeleteProgram,CreateOperational,UpdateOperational,DeleteOperational'
    ), (
        5,
        'Fundraising',
        'ReadCOD,CreateCOD,UpdateCOD,DeleteCOD'
    ), (
        6,
        'Manajer',
        'CreateIncome,UpdateIncome,DeleteIncome,CreateExpense,UpdateExpense,DeleteExpense,CreateFinancialReport,UpdateFinancialReport,DeleteFinancialReport,CreateArusKas,UpdateArusKas,DeleteArusKas,CreateProgram,UpdateProgram,DeleteProgram,CreateOperational,UpdateOperational,DeleteOperational,ReadCOD,CreateCOD,UpdateCOD,DeleteCOD'
    );

INSERT INTO auth_user_role_comp (id) VALUES (570500911);

INSERT INTO
    auth_user_role_impl(id, authrole, authuser)
VALUES (570500911, 1, 487438608);

------------------------

-- ACTIVITY SEED

------------------------

INSERT INTO program_comp (idprogram) VALUES (1), (2), (3), (4);

INSERT INTO
    program_impl(
        name,
        executiondate,
        target,
        idprogram,
        description,
        logourl,
        partner
    )
VALUES (
        'Charity Run: Berlari untuk Donasi',
        '2023-01-14',
        'Kotak makanan untuk pemulung',
        1,
        'Sehatkan diri dengan berlari, sehatkan hati dengan berbagi. Waste Free Bogor mengadakan Charity Run perdana, bekerja sama dengan Bogor Runners, dan mengajak kita semua untuk berlari bersama sekaligus berbagi rezeki pada pemulung-pemulung di sekitar kota.',
        'https://images.unsplash.com/photo-1613937574892-25f441264a09?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80.jpg',
        'Bogor Runners'
    ), (
        'Kelola Sampah Desa Cikarawang',
        '2023-02-15',
        'Desa Cikarawang',
        2,
        'Cegah warga Desa Cikarawang membakar sampah di pinggir jalan dan membuang sampah di kali.',
        'https://images.unsplash.com/photo-1485504750689-aa2121ffe6c6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1208&q=80.jpg',
        'Pemda Desa Cikarawang'
    ), (
        'Peduli Keluarga Pemulung',
        '2023-02-01',
        'Keluarga Pemulung Kota Bogor',
        3,
        'Berikan beasiswa untuk anak-anak keluarga pemulung.',
        'https://images.unsplash.com/photo-1657101736083-92e59db017a2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80.jpg',
        'UNICEF'
    ), (
        'Bersih-Bersih Bersama: Terminal Baranangsiang',
        '2023-01-06',
        'Terminal Baranangsiang',
        4,
        'Bersama kita bersihkan Terminal Baranangsiang untuk selangkah lebih dekat menuju Bogor Tanpa Sampah!',
        'https://statik.tempo.co/data/2013/05/02/id_181424/181424_620.jpg',
        '-'
    );

------------------------

-- CHART OF ACCOUNTS SEED

------------------------

INSERT INTO
    public.chartofaccount_comp (id)
VALUES (10000), (11000), (11100), (11200), (12000), (12100), (12200), (19000), (40000), (41000), (41010), (41020), (42000), (42010), (42020), (43000), (43010), (43020), (43099), (44000), (49000), (60000), (61000), (62000), (63000), (63400), (64000), (65000), (65100), (65200), (65400), (66000), (66100), (66200), (66300), (67000), (68000), (69000);

INSERT INTO
    public.chartofaccount_impl (
        id,
        code,
        name,
        description,
        isVisible
    )
VALUES (10000, 10000, 'Saldo', '    ', 0), (
        11000,
        11000,
        'Rekening Bank',
        '    ',
        0
    ), (
        11100,
        11100,
        'Rekening Bank - Khusus',
        'Saldo yang tersimpan di rekening bank, untuk kebutuhan/pembiayaan khusus, seperti rekening untuk zakat fithrah, hanya untuk fakir miskin saja.',
        1
    ), (
        11200,
        11200,
        'Rekening Bank - Umum',
        'Saldo yang tersimpan di rekening bank, dan memang jelas bebas digunakan untuk institusi sesuai aturan yang berlaku,',
        1
    ), (12000, 12000, 'Kas', '    ', 0), (
        12100,
        12100,
        'Kas - Umum',
        'Saldo yang disimpan pengurus, untuk kebutuhan umum seperti saldo kotak amal Jumat',
        1
    ), (
        12200,
        12200,
        'Kas - Khusus',
        'Saldo yang disimpan pengurus, untuk kebutuhan khusus seperti saldo kas untuk operasional dan konsumsi petugas',
        1
    ), (
        19000,
        19000,
        'Saldo Lainnya',
        'Saldo lainnya dalam bentuk non cash yang dihargai dalam rupiah.',
        0
    ), (
        40000,
        40000,
        'Penerimaan',
        '    ',
        0
    ), (
        41000,
        41000,
        'Donasi Institusi',
        '    ',
        0
    ), (
        41010,
        41010,
        'Donasi Pemerintah',
        'Donasi dari pemerintah misalnya seperti BLT',
        1
    ), (
        41020,
        41020,
        'Donasi Swasta (CSR)',
        'Donasi dari swasta seperti dari dana CSR atau sumbangan karyawan.',
        1
    ), (
        42000,
        42000,
        'Donasi Individu',
        '    ',
        0
    ), (
        42010,
        42010,
        'Donasi Individu Tercatat',
        'Donasi dari individu yang tercatat sumber dan identitas pengirim',
        1
    ), (
        42020,
        42020,
        'Donasi Anonymous',
        'Donasi dari individu yang tidak diketahui sumber nya atau dinyatakan anonymous.',
        1
    ), (
        43000,
        43000,
        'Penerimaan Hasil Usaha',
        'Rekapitulasi segala Penerimaan institusi dari hasil usaha baik usaha internal maupun kerjasama',
        0
    ), (
        43010,
        43010,
        'Hasil penjualan',
        'Penerimaan institusi dari kegiatan penjualan barang',
        1
    ), (
        43020,
        43020,
        'Hasil upah jasa',
        'Penerimaan institusi dari kegiatan upah atau jasa',
        1
    ), (
        43099,
        43099,
        'Bonus/ Hasil usaha lain-lain',
        'Penerimaan institusi dari usaha lain-lain atau bonus/komisi',
        1
    ), (
        44000,
        44000,
        'Iuran',
        'Penerimaan dalam bentuk iuran rutin, seperti iuran kebersihan.',
        1
    ), (
        49000,
        49000,
        'Penerimaan Lainnya',
        'Penerimaan yang belum bisa dikategorikan.',
        1
    ), (
        60000,
        60000,
        'Pengeluaran',
        '    ',
        0
    ), (
        61000,
        61000,
        'Penyaluran donasi',
        'Pembiayaan dalam bentuk menyalurkan kembali donasi yang diterima kepada pihak lain, seperti penyaluran zakat maal kepada para mustahiq.',
        1
    ), (
        62000,
        62000,
        'Gaji dan Honor',
        'Pembiayaan untuk gaji dan honor, seperti gaji marbot, gaji keamanan, honor pembicaran, honor ustadz, moderator',
        1
    ), (
        63000,
        63000,
        'Biaya Umum',
        'Biaya untuk kebutuhan umum seperti administrasi, iuran kebersihan, dan keamanan',
        0
    ), (
        63400,
        63400,
        'Biaya Administrasi',
        'Pembiayaan administrasi seperti biaya pengurusan akta notaris, biaya transfer dll',
        1
    ), (
        64000,
        64000,
        'Biaya Sewa dan Jasa',
        'Pembiayaan sewa ruangan, alat, sewa jasa, termasuk jasa biaya tranportasi, kurir, akomodasi',
        1
    ), (
        65000,
        65000,
        'Pengeluaran Kegiatan/Program',
        'Pengeluaran yang terkait dengan kegiatan atau program institusi',
        1
    ), (
        65100,
        65100,
        'Biaya Publikasi',
        'Pembiayaan untuk publikasi program, kegiatan',
        1
    ), (
        65200,
        65200,
        'Biaya Konsumsi',
        'Pembiayaan untuk konsumsi kegiatan/program',
        1
    ), (
        65400,
        65400,
        'Biaya Peralatan',
        'Pembiayaan pembeliaan peralatan untuk kegiatan atau untuk kepentingan ins',
        1
    ), (
        66000,
        66000,
        'Pengeluaran Umum/Rutin',
        'Segala pembiayaan/ pengeluaran yang rutin terjadi atau tidak terikat program/kegiatan khusus',
        1
    ), (
        66100,
        66100,
        'Biaya Bahan Habis Pakai',
        'Pembiayaan bahan habis pakai yang rutin digunakan, seperti printer, sabun, cairan pembersih.',
        1
    ), (
        66200,
        66200,
        'Biaya Operasional',
        'Pembiayaan rutin operasional, seperti listrik, air, internet',
        1
    ), (
        66300,
        66300,
        'Biaya Perawatan',
        'Pembiayaan terkait perawatan aset institusi, termasuk cat tembok rutin.',
        1
    ), (
        67000,
        67000,
        'Pengembangan SDM/ Pelatihan',
        'Pembiayaan Pengembangan Sumber daya Insitutusi, seperti pelatihan, beasiswa, buku.',
        1
    ), (
        68000,
        68000,
        'Biaya Renovasi/ Reparasi',
        'Pembiayaan renovasi, perbaikan kerusakan bangunan, alat, kendaraan',
        1
    ), (
        69000,
        69000,
        'Biaya-biaya Lainnya',
        'Pembiayaan lain yang belum terkategorikan atau belum jelas kategorinya',
        1
    );

------------------------

-- INCOME SEED

------------------------

insert into
    financialreport_comp (id)
values (1208481968), (1227818463), (2007275122), (676728937), (1458769084), (90514058), (669340556), (1501706140), (1152105770), (1228000425), (1700031801), (1071803574), (469388498), (188947932), (21808274), (1476262257), (198373970), (1754518115);

INSERT INTO
    financialreport_impl (
        amount,
        datestamp,
        description,
        id,
        coa_id,
        program_idprogram
    )
VALUES (
        100000,
        '2023-01-01',
        'HTM Charity Run',
        1208481968,
        42000,
        1
    ), (
        80000,
        '2023-01-01',
        'HTM Charity Run',
        2007275122,
        42000,
        1
    ), (
        120000,
        '2023-01-02',
        'HTM Charity Run',
        1458769084,
        42000,
        1
    ), (
        500000,
        '2023-01-03',
        'HTM Charity Run   Donasi',
        669340556,
        41020,
        1
    ), (
        1200000,
        '2023-01-03',
        'Sponsor Pemkot Bogor',
        1152105770,
        41010,
        2
    ), (
        3500000,
        '2023-01-01',
        'Donasi Bank BRI',
        1700031801,
        41000,
        3
    ), (
        3900000,
        '2023-01-03',
        'Donasi Bank Mega',
        469388498,
        41000,
        3
    ), (
        4400000,
        '2023-01-03',
        'Donasi PT Gojek',
        21808274,
        41020,
        3
    ), (
        2000000,
        '2023-01-03',
        'Donasi PT Astra Internasional',
        198373970,
        41020,
        4
    );

INSERT INTO
    financialreport_income (paymentmethod, id, record_id)
VALUES (
        "Transfer Bank",
        1227818463,
        1208481968
    ), ("Ovo", 676728937, 2007275122), (
        "Transfer Bank",
        90514058,
        1458769084
    ), (
        "Transfer Bank",
        1501706140,
        669340556
    ), (
        "Transfer Bank",
        1228000425,
        1152105770
    ), (
        "Transfer Bank",
        1071803574,
        1700031801
    ), (
        "Transfer Bank",
        188947932,
        469388498
    ), ("Gopay", 1476262257, 21808274), (
        "Transfer Bank",
        1754518115,
        198373970
    );